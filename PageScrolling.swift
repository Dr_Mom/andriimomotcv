//
//  PageScrolling.swift
//  myCV
//
//  Created by Andrei Momot on 12/10/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

protocol PageScrolling {
  /**
   Update view when a part of it is on the left.
   @param offset Must be a value between 0 and 1 (0 meaning page is fully visible, 
   1 meaning page is completely to the left of the visible page)
   */
  func onScrollWithPageLeft(_ offset: CGFloat)

  /**
   Update view when a part of it is on the right page currently visible.
   @param offset Must be a value between 0 and 1 (0 meaning page is fully visible, 
   1 meaning page is completely to the right of the visible page)
   */
  func onScrollWithPageRight(_ offset: CGFloat)
}
