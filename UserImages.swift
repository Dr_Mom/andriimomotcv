//
//  Images.swift
//  myCV
//
//  Created by Andrei Momot on 2/22/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

struct UserImages {
  var profileImageURL: String?
  var battleShipImageURL: String?
  var recipeImageURL: String?
  var pokerImageURL: String?
  var flappyBirdImageURL: String?
  var calculatorImageURL: String?

  init(dictionary: [String: Any]) {
    self.profileImageURL = dictionary["profileURL"] as? String ?? ""
    self.battleShipImageURL = dictionary["battleShipURL"] as? String ?? ""
    self.recipeImageURL = dictionary["recipeURL"] as? String ?? ""
    self.flappyBirdImageURL = dictionary["flappyBirdURL"] as? String ?? ""
    self.calculatorImageURL = dictionary["calculatorURL"] as? String ?? ""
    self.pokerImageURL = dictionary["pokerURL"] as? String ?? ""
  }
}
