//
//  EduIntroViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/12/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class EduIntroViewController: ChildViewController {
  @IBOutlet var plainLabels: [UILabel]!
  @IBOutlet weak var university: UILabel!
  @IBOutlet weak var speciality: UILabel!
  @IBOutlet weak var graduationDate: UILabel!

  var userInfo: UserInfo?
  var service = Service()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.university.text = userInfo.univercity
      self.speciality.text = userInfo.speciality
      self.graduationDate.text = userInfo.graduationDate
    }
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    for label in plainLabels {
      label.transform = CGAffineTransform(translationX: self.view.width * offset, y: 0)
      label.alpha = 1 - offset
    }
  }
}
