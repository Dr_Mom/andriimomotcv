//
//  AboutMeIntroViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/9/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class AboutMeIntroViewController: ChildViewController {
  var userInfo: UserInfo?
  var images: UserImages?
  var service = Service()
  var profileImgURL: String?

  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var CVImageView: UIImageView!
  @IBOutlet weak var devLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
    configureImageView()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.headerLabel.text = userInfo.aboutMeHeader
      self.nameLabel.text = userInfo.name
      self.devLabel.text = userInfo.dev
    }
    headerLabel.configurePlainLabel()
    nameLabel.configureLargeLabel()
    devLabel.configureSmallLabel()
  }

  private func configureImageView() {
    service.getImages { (images) in
      self.profileImgURL = images.profileImageURL
      if let imageURL: URL = URL(string: self.profileImgURL!) {
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.CVImageView.image = UIImage(data: imageData as Data)
      }
    }
    CVImageView.roundedImage()
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.headerLabel.transform = CGAffineTransform(translationX: 0, y: -(self.view.frame.height * 0.5 * offset))
    self.nameLabel.transform = CGAffineTransform(translationX: 0, y: -(self.view.frame.height * 0.3 * offset))
    self.CVImageView.transform = CGAffineTransform(scaleX: 1 + offset, y: 1 + offset)
    self.CVImageView.alpha = 1 - offset
    self.devLabel.transform = CGAffineTransform(translationX: 0, y: self.view.frame.height * 0.3 * offset)
  }

  override func onScrollWithPageRight(_ offset: CGFloat) {
    let newAlpha = 1 - offset
    self.headerLabel.alpha = newAlpha
    self.nameLabel.alpha = newAlpha
    self.CVImageView.alpha = newAlpha
    self.devLabel.alpha = newAlpha
  }
}
