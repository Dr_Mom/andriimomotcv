//
//  EduEnglishViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/13/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class EduEnglishViewController: ChildViewController {
  @IBOutlet var plainLabels: [UILabel]!
  @IBOutlet weak var english1: UILabel!
  @IBOutlet weak var english2: UILabel!
  @IBOutlet weak var english3: UILabel!

  var userInfo: UserInfo?
  var service = Service()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.english1.text = userInfo.english1
      self.english2.text = userInfo.english2
      self.english3.text = userInfo.english3
    }
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    for label in plainLabels {
      label.transform = CGAffineTransform(translationX: 0, y: self.view.height * offset)
      label.alpha = 1 - offset
    }
      english1.transform = CGAffineTransform(translationX: 0, y: self.view.height * -offset)
      english2.transform = CGAffineTransform(translationX: 0, y: self.view.height * -offset)
      english3.transform = CGAffineTransform(translationX: 0, y: self.view.height * -offset)
  }
}
