//
//  SkillsViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/14/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit
import PKHUD

class SkillsViewController: ChildViewController {
  fileprivate enum CellIdentifier: String {
    case portfolioCell = "PortfolioItemTableViewCell"
  }

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var titleLabel: UILabel!

  var userInfo: UserInfo?
  var userSkills: UserSkills?
  var service = Service()
  var skills = [String]()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureSkills()
  }

  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.titleLabel.text = userInfo.skillTitle
    }
    titleLabel.configurePlainLabel()
  }

  private func configureSkills() {
    service.getSkills { (userSkills) in
      self.configureTableView()
      self.skills.append(userSkills.userSkill)
      self.tableView.reloadData()
    }
  }

  @IBAction func addButtonTapped(_ sender: Any) {
    let alert = UIAlertController(title: "Skill", message: "Add an Item", preferredStyle: .alert)
    let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
      guard let textField = alert.textFields?.first, let skill = textField.text else { return }
      self.service.postSkill(text: skill)
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .default)

    alert.addTextField()

    alert.addAction(saveAction)
    alert.addAction(cancelAction)

    present(alert, animated: true, completion: nil)
  }
}

extension SkillsViewController: UITableViewDataSource {
  fileprivate func configureTableView() {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    self.tableView.backgroundColor = UIColor.clear
    self.tableView.indicatorStyle = UIScrollViewIndicatorStyle.white
    self.tableView.register(UINib(nibName: CellIdentifier.portfolioCell.rawValue, bundle: nil),
                            forCellReuseIdentifier: PortfolioItemTableViewCell.cellIdentifier)
    self.tableView.rowHeight = 60
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: PortfolioItemTableViewCell.cellIdentifier, for: indexPath)
    cell.textLabel!.text = self.skills[indexPath.row]
    return cell
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.skills.count
  }

  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle,
                 forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      let skill = skills[indexPath.row]
      self.service.removeSkill(text: skill)
      self.skills.remove(at: indexPath.row)
      tableView.reloadData()
    }
  }
}
