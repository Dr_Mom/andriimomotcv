//
//  AboutAppViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/15/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class AboutAppViewController: ChildViewController {
  var userInfo: UserInfo?
  var service = Service()

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.titleLabel.text = userInfo.aboutAppHeader
      self.descriptionLabel.text = userInfo.aboutApp
    }
    titleLabel.configurePlainLabel()
    descriptionLabel.configureSmallLabel()
  }
}
