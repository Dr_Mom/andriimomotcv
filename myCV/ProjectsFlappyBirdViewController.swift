//
//  ProjectsFlappyBirdViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/14/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class ProjectsFlappyBirdViewController: ChildViewController {
  var images: UserImages?
  var userInfo: UserInfo?
  var service = Service()
  var flappyBirdImgURL: String?

  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureImageView()
  }

  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.descriptionLabel.text = userInfo.flappyBird
    }
    descriptionLabel.configurePlainLabel()
  }

  private func configureImageView() {
    service.getImages { (images) in
      self.flappyBirdImgURL = images.flappyBirdImageURL
      if let imageURL: URL = URL(string: self.flappyBirdImgURL!) {
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.logoImageView.image = UIImage(data: imageData as Data)
      }
    }
    logoImageView.roundedCornerImage()
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(translationX: offset * self.view.width,
                                                     y: -offset * self.view.height)
    self.descriptionLabel.transform = CGAffineTransform(translationX: offset * self.view.width,
                                                        y: offset * self.view.height)
  }
  
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(scaleX: 1 - offset, y: 1 - offset)
    self.descriptionLabel.alpha = 1 - offset
  }
}
