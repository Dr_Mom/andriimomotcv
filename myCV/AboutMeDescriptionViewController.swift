//
//  AboutMeDescriptionViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/12/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class AboutMeDescriptionViewController: ChildViewController {
  var userInfo: UserInfo?
  var service = Service()

  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.descriptionLabel.text = userInfo.aboutMe
    }
    descriptionLabel.configurePlainLabel()
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.descriptionLabel.transform = CGAffineTransform(scaleX: 1 - offset, y: 1 - offset)
  }

  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.descriptionLabel.transform = CGAffineTransform(translationX: offset * self.view.width,
                                                        y: offset * self.view.height)
  }
}
