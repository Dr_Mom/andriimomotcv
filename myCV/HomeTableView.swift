//
//  HomeTableViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/9/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
  private enum PortfolioIdentifier: String {
    case aboutMe = "About Me"
    case background = "Background"
    case projects = "Projects"
    case skills = "Skills"
    case contact = "Contact"
  }

  private enum VCIdentifier: String {
    case aboutIntroVC = "AboutMeIntroVC"
    case aboutDescriptionVC = "AboutMeDescriptionVC"
    case aboutHobbiesVC = "AboutMeHobbiesVC"
    case eduIntroVC = "EduIntroVC"
    case eduITVC = "EduITVC"
    case eduEngVC = "EduEngVC"
    case projectsBattleVC = "ProjectsBattleVC"
    case projectsRecipeVC = "ProjectsRecipeVC"
    case projectsCalcVC = "ProjectsCalcVC"
    case projectsFlappyVC = "ProjectsFlappyVC"
    case projectsPokerVC = "ProjectsPokerVC"
    case skillsVC = "SkillsVC"
    case contactVC = "ContactVC"
    case aboutVC = "AboutVC"
    case rootContainerVC = "RootContainerVC"
  }

  private enum PortfolioType: Int {
    case about = 0
    case background = 1
    case projects = 2
    case skills = 3
    case contact = 4
  }

  private enum Image: String {
    case night = "night.jpg"
    case winter = "winter.jpg"
    case city = "city.jpg"
    case stadium = "stadium.jpg"
    case tram = "tram.jpg"
  }

  private var normalCellColor: UIColor {
    return UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
  }

  private var highlightedCellColor: UIColor {
    return UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
  }

  private func stringForPortfolioType(_ type: PortfolioType) -> String {
    let type = type

    switch type {
    case .about:
      return PortfolioIdentifier.aboutMe.rawValue
    case .background:
      return PortfolioIdentifier.background.rawValue
    case .projects:
      return PortfolioIdentifier.projects.rawValue
    case .skills:
      return PortfolioIdentifier.skills.rawValue
    case .contact:
      return PortfolioIdentifier.contact.rawValue
    }
  }

  private func pages(_ type: PortfolioType) -> [ChildViewController] {
    let type = type

    switch type {
    case .about:
      let aboutIntroVC = pageChildViewController(VCIdentifier.aboutIntroVC.rawValue)
      let aboutDescriptionVC = pageChildViewController(VCIdentifier.aboutDescriptionVC.rawValue)
      let aboutHobbiesVC = pageChildViewController(VCIdentifier.aboutHobbiesVC.rawValue)
      return [aboutIntroVC, aboutDescriptionVC, aboutHobbiesVC]
    case .background:
      let eduIntroVC = pageChildViewController(VCIdentifier.eduIntroVC.rawValue)
      let eduITVC = pageChildViewController(VCIdentifier.eduITVC.rawValue)
      let eduEngVC = pageChildViewController(VCIdentifier.eduEngVC.rawValue)
      return [eduIntroVC, eduITVC, eduEngVC]
    case .projects:
      let projectsBattleVC = pageChildViewController(VCIdentifier.projectsBattleVC.rawValue)
      let projectsRecipeVC = pageChildViewController(VCIdentifier.projectsRecipeVC.rawValue)
      let projectsCalcVC = pageChildViewController(VCIdentifier.projectsCalcVC.rawValue)
      let projectsFlappyVC = pageChildViewController(VCIdentifier.projectsFlappyVC.rawValue)
      let projectsPokerVC = pageChildViewController(VCIdentifier.projectsPokerVC.rawValue)
      return [projectsBattleVC, projectsRecipeVC, projectsCalcVC, projectsFlappyVC, projectsPokerVC]
    case .skills:
      let skillsVC = pageChildViewController(VCIdentifier.skillsVC.rawValue)
      return [skillsVC]
    case .contact:
      let contactVC = pageChildViewController(VCIdentifier.contactVC.rawValue)
      let aboutVC = pageChildViewController(VCIdentifier.aboutVC.rawValue)
      return [contactVC, aboutVC]
    }
  }

  private func backgroundImage(_ type: PortfolioType) -> UIImage {
    let type = type

    switch type {
    case .about:
      guard let bgNight = UIImage(named: Image.night.rawValue) else {
        return UIImage()
      }
      return bgNight
    case .background:
      guard let bgWinter = UIImage(named: Image.winter.rawValue) else {
        return UIImage()
      }
      return bgWinter
    case .projects:
      guard let bgCity = UIImage(named: Image.city.rawValue) else {
        return UIImage()
      }
      return bgCity
    case .skills:
      guard let bgStadium = UIImage(named: Image.stadium.rawValue) else {
        return UIImage()
      }
      return bgStadium
    case .contact:
      guard let bgTram = UIImage(named: Image.tram.rawValue) else {
        return UIImage()
      }
      return bgTram
    }
  }

  private func pageChildViewController(_ identifier: String) -> ChildViewController {
    guard let shownStoryboard = self.storyboard!.instantiateViewController(withIdentifier:
      identifier) as? ChildViewController else {
      fatalError()
    }
    return shownStoryboard
  }

  func configureCell(_ cell: PortfolioTableViewCell, forRow row: Int) {
    cell.textLabel?.text = stringForPortfolioType(PortfolioType(rawValue: row)!).uppercased()
    cell.textLabel?.textAlignment = NSTextAlignment.center
    cell.textLabel?.textColor = UIColor.white
    cell.textLabel?.font = UIFont(name: "Avenir-Light", size: 18)
    cell.textLabel?.backgroundColor = UIColor.clear
    cell.backgroundColor = self.normalCellColor
    cell.layoutMargins = UIEdgeInsets.zero

    let selectedView = UIView()
    selectedView.backgroundColor = self.highlightedCellColor
    cell.selectedBackgroundView = selectedView
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell: PortfolioTableViewCell = tableView.dequeueReusableCell(withIdentifier:
      PortfolioTableViewCell.cellIdentifer, for: indexPath) as? PortfolioTableViewCell else {
      fatalError()
    }
    configureCell(cell, forRow: indexPath.row)
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    let type: PortfolioType = PortfolioType(rawValue: indexPath.row)!

    guard let containerVC: RootContainerViewController = self.storyboard!.instantiateViewController(withIdentifier:
      VCIdentifier.rootContainerVC.rawValue) as? RootContainerViewController else {
      fatalError()
    }

    containerVC.backgroundImage = self.backgroundImage(type)
    containerVC.pages = self.pages(type)

    if InternetReachability.isConnectedToNetwork() == true {
      self.present(containerVC, animated: true, completion: nil)
    }
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return (self.cellView.frame.size.height - self.defaultHeaderHeight) / 5
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
}
