//
//  RootContainerViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/10/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class RootContainerViewController: UIViewController {
  @IBOutlet weak var bgImageView: UIImageView!
  @IBOutlet weak var scrollRightImageView: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!

  var backgroundImage: UIImage?

  fileprivate let imageSideOffset: CGFloat = 50

  fileprivate let imageShiftRatio: CGFloat = 0.25

  var pages = [UIViewController]()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureBackgroundImage()
    configureScrollView()
    self.view.clipsToBounds = true
    if self.pages.count == 0 {
      for _ in 0...5 {
        let childVC = ChildViewController()
        self.addChildViewController(childVC)
        self.scrollView.addSubview(childVC.view)
        childVC.didMove(toParentViewController: self)
        self.pages.append(childVC)
      }
    } else {
      for i in 0..<self.pages.count {
        let childVC = self.pages[i]
        self.addChildViewController(childVC)
        self.scrollView.addSubview(childVC.view)
        childVC.didMove(toParentViewController: self)
      }
    }
    if self.pages.count == 1 {
      self.scrollRightImageView.alpha = 0
    } else {
      self.scrollRightImageView.alpha = 1
    }
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    self.scrollView.frame = self.view.frame
    self.scrollView.contentSize.width = CGFloat(self.pages.count) * self.view.frame.width
    self.scrollView.contentSize.height = self.view.frame.height

    for i in 0..<self.pages.count {
      pages[i].view.frame = CGRect(x: CGFloat(i) * self.view.width,
                                   y: 0,
                                   width: self.view.frame.width,
                                   height: self.view.frame.height)
    }

    let aspectRatio: CGFloat = self.bgImageView.frame.height / self.bgImageView.frame.width

    var imageWidth: CGFloat = imageShiftRatio * (self.scrollView.contentSize.width - self.view.frame.width) +
                                                (imageSideOffset * 2) + self.view.frame.width

    var imageHeight: CGFloat = imageWidth * aspectRatio

    if imageHeight <= self.view.frame.height {
      imageHeight = self.view.frame.height
      imageWidth = imageHeight / aspectRatio
    }

    self.bgImageView.frame = CGRect(x: -scrollView.contentOffset.x - imageSideOffset,
                                    y: -((imageHeight / 2.0) - (self.view.frame.height / 2.0)) / 2.0,
                                    width: imageWidth,
                                    height: imageHeight)
  }

  private func configureBackgroundImage() {
    self.bgImageView.image = self.backgroundImage
    self.bgImageView.contentMode = UIViewContentMode.scaleAspectFill
  }

  @IBAction func dismissButton(_ sender: AnyObject) {
    self.dismiss(animated: true, completion: nil)
  }
}

extension RootContainerViewController: UIScrollViewDelegate {
  fileprivate func configureScrollView() {
    self.scrollView.isPagingEnabled = true
    self.scrollView.backgroundColor = UIColor(white: 0, alpha: 0.5)
    self.scrollView.indicatorStyle = UIScrollViewIndicatorStyle.white
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let offsetX: CGFloat = scrollView.contentOffset.x

    let positionX: CGFloat = offsetX / self.view.frame.width

    self.scrollRightImageView.alpha = 1 - (offsetX * 0.01)

    let leftPageIndex: Int = Int(floor(positionX))
    let rightPageIndex: Int = leftPageIndex + 1

    let leftOffset: CGFloat = positionX - floor(positionX)
    let rightOfsset: CGFloat = 1 - leftOffset

    if leftPageIndex >= 0 {
      guard let leftIndex = pages[leftPageIndex] as? PageScrolling else {
        fatalError()
      }
      leftIndex.onScrollWithPageLeft(leftOffset)
    }

    if rightPageIndex < self.pages.count {
      guard let rightIndex = pages[rightPageIndex] as? PageScrolling else {
          fatalError()
        }
      rightIndex.onScrollWithPageRight(rightOfsset)
    }

    let imageLeftAdjust: CGFloat = -offsetX * imageShiftRatio - imageSideOffset
    if imageLeftAdjust < 0 && imageLeftAdjust + self.bgImageView.frame.width > self.view.frame.width {
      self.bgImageView.left = imageLeftAdjust
    }
  }
}
