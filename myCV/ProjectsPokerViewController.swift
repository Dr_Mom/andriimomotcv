//
//  ProjectsPokerViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/14/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class ProjectsPokerViewController: ChildViewController {
  var userInfo: UserInfo?
  var images: UserImages?
  var service = Service()
  var pokerImgURL: String?

  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureImageView()
  }

  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.descriptionLabel.text = userInfo.poker
    }
    descriptionLabel.configurePlainLabel()
  }

  private func configureImageView() {
    service.getImages { (images) in
      self.pokerImgURL = images.pokerImageURL
      if let imageURL: URL = URL(string: self.pokerImgURL!) {
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.logoImageView.image = UIImage(data: imageData as Data)
      }
    }
    logoImageView.roundedCornerImage()
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(rotationAngle: offset * CGFloat.pi)
  }

  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(rotationAngle: -offset * CGFloat.pi)
    self.descriptionLabel.transform = CGAffineTransform(translationX: offset * self.view.width,
                                                        y: offset * self.view.height)
  }
}
