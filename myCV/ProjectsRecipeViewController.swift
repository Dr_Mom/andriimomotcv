//
//  ProjectsRecipeViewController.swift
//  myCV
//
//  Created by Andrei Momot on 3/14/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

class ProjectsRecipeViewController: ChildViewController {
  var images: UserImages?
  var userInfo: UserInfo?
  var service = Service()
  var recipeImgURL: String?
  
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureImageView()
  }
  
  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.descriptionLabel.text = userInfo.recipe
    }
    descriptionLabel.configurePlainLabel()
  }
  
  private func configureImageView() {
    service.getImages { (images) in
      self.recipeImgURL = images.recipeImageURL
      if let imageURL: URL = URL(string: self.recipeImgURL!) {
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.logoImageView.image = UIImage(data: imageData as Data)
      }
    }
    logoImageView.roundedCornerImage()
  }
  
  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(scaleX: 1 - offset, y: 1 - offset)
    self.descriptionLabel.alpha = 1 - offset
  }
  
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(translationX: offset * self.view.width,
                                                     y: -offset * self.view.height)
    self.descriptionLabel.transform = CGAffineTransform(translationX: offset * self.view.width,
                                                        y: offset * self.view.height)
  }
}
