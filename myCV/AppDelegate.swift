//
//  AppDelegate.swift
//  myCV
//
//  Created by Andrei Momot on 12/8/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    FIRApp.configure()
    FIRDatabase.database().persistenceEnabled = true
    return true
  }
}
