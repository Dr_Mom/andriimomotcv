//
//  ContactViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/15/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class ContactViewController: ChildViewController {
  private enum Link: String {
    case skypeInstalled = "skype"
    case skypeAdd = "skype:JZ_Mom"
    case skypeDownload = "https://itunes.apple.com/ua/app/skype-for-iphone/id304878510?mt=8"
    case linkedIn = "https://www.linkedin.com/in/andreymomot"
    case facebook = "https://www.facebook.com/MomotAndrii"
    case bitbucket = "https://bitbucket.org/Dr_Mom"
    case github = "https://github.com/AndreyMomot"
  }

  @IBOutlet var buttons: [ActionButton]!

  func openSkype() {
    let installed = UIApplication.shared.canOpenURL(NSURL(string: Link.skypeInstalled.rawValue)! as URL)
    if installed {
      UIApplication.shared.open((NSURL(string: Link.skypeAdd.rawValue)! as URL), options: [:], completionHandler: nil)
    } else {
      UIApplication.shared.open((NSURL(string: Link.skypeDownload.rawValue)! as URL),
                                options: [:], completionHandler: nil)
    }
  }

  @IBAction func goToSkype(_ sender: AnyObject) {
    openSkype()
  }

  @IBAction private func goToLinkedIn(_ sender: AnyObject) {
    self.goToLink(scheme: Link.linkedIn.rawValue)
  }

  @IBAction private func goToFacebook(_ sender: AnyObject) {
    self.goToLink(scheme: Link.facebook.rawValue)
  }

  @IBAction private func goToBitbucket(_ sender: AnyObject) {
    self.goToLink(scheme: Link.bitbucket.rawValue)
  }

  @IBAction private func goToGitHub(_ sender: AnyObject) {
    self.goToLink(scheme: Link.github.rawValue)
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    for i in 0..<self.buttons.count {
      let button = self.buttons[i]
      button.transform = CGAffineTransform(rotationAngle: offset * CGFloat(i) * 0.4)
    }
  }
}
