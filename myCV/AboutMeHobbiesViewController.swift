//
//  AboutMeHobbiesViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/12/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class AboutMeHobbiesViewController: ChildViewController {
  fileprivate enum CellIdentifier: String {
    case portfolioCell = "PortfolioItemTableViewCell"
  }

  var userInfo: UserInfo?
  var userHobbies: UserHobbies?
  var service = Service()

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var titleButtonSpace: NSLayoutConstraint!
  @IBOutlet weak var titleTableViewSpace: NSLayoutConstraint!

  var rowHeight: CGFloat = 70
  var hobbies = [String]()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureHobbies()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    if self.dismissButtonOffset + self.titleLabel.height +
       self.titleTableViewSpace.constant + self.titleButtonSpace.constant +
       (CGFloat(self.hobbies.count) * rowHeight) > self.view.height {
      self.titleButtonSpace.constant = self.dismissButtonOffset
      self.titleTableViewSpace.constant = 10
    }
    if rowHeight * CGFloat(self.hobbies.count) + self.titleLabel.height + self.titleLabel.top > self.view.height {
      self.tableView.rowHeight = 50
    }
  }

  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.titleLabel.text = userInfo.aboutMeTitle
    }
     titleLabel.configurePlainLabel()
  }

  private func configureHobbies() {
    service.getHobbies { (userHobbies) in
      self.configureTableView()
      self.hobbies.append(userHobbies.userHobbie!)
      self.tableView.reloadData()
    }
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.titleLabel.transform = CGAffineTransform(translationX: -offset * self.view.width,
                                                  y: -offset/2 * self.view.height)

    for i in 0..<self.tableView.visibleCells.count {
      let cell = self.tableView.visibleCells[i]
      cell.transform = CGAffineTransform(translationX: CGFloat(i) * offset * 150, y: 0)
    }
  }

  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.titleLabel.alpha = 1 - offset

    for i in 0..<self.tableView.visibleCells.count {
      let cell = self.tableView.visibleCells[i]
      cell.transform = CGAffineTransform(translationX: CGFloat(i) * offset * -150, y: 0)
    }
  }
}

extension AboutMeHobbiesViewController: UITableViewDataSource {
  fileprivate func configureTableView() {
    self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    self.tableView.backgroundColor = UIColor.clear
    self.tableView.tableFooterView = UIView()
    self.tableView.register(UINib(nibName: CellIdentifier.portfolioCell.rawValue, bundle: nil),
                            forCellReuseIdentifier: PortfolioItemTableViewCell.cellIdentifier)
    self.tableView.rowHeight = self.rowHeight
    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    self.tableView.isUserInteractionEnabled = false
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: PortfolioItemTableViewCell.cellIdentifier, for: indexPath)
    cell.textLabel!.text = self.hobbies[indexPath.row]
    return cell
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.hobbies.count
  }
}
