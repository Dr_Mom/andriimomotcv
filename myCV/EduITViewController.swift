//
//  EduITViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/13/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class EduITViewController: ChildViewController {
  @IBOutlet var plainLabels: [UILabel]!
  @IBOutlet weak var course1: UILabel!
  @IBOutlet weak var course2: UILabel!
  @IBOutlet weak var course3: UILabel!

  var userInfo: UserInfo?
  var service = Service()

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.course1.text = userInfo.course1
      self.course2.text = userInfo.course2
      self.course3.text = userInfo.course3
    }
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    for label in plainLabels {
      label.transform = CGAffineTransform(translationX: self.view.width * offset, y: 0)
      label.alpha = 1 - offset
    }
  }
}
