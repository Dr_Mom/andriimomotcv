//
//  ProjectsCalculatorViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/14/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit

class ProjectsCalculatorViewController: ChildViewController {
  var images: UserImages?
  var userInfo: UserInfo?
  var service = Service()
  var calculatorImgURL: String?

  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureImageView()
  }

  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.descriptionLabel.text = userInfo.calculator
    }
    descriptionLabel.configurePlainLabel()
  }

  private func configureImageView() {
    service.getImages { (images) in
      self.calculatorImgURL = images.calculatorImageURL
      if let imageURL: URL = URL(string: self.calculatorImgURL!) {
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.logoImageView.image = UIImage(data: imageData as Data)
      }
    }
    logoImageView.roundedCornerImage()
  }

  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(rotationAngle: offset * CGFloat.pi)
  }
  
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(rotationAngle: -offset * CGFloat.pi)
  }
}
