//
//  ProjectsBattleShipViewController.swift
//  myCV
//
//  Created by Andrei Momot on 3/14/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit
import PKHUD

class ProjectsBattleShipViewController: ChildViewController {
  var images: UserImages?
  var userInfo: UserInfo?
  var service = Service()
  var battleShipImgURL: String?
  
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabel()
    configureImageView()
  }
  
  private func configureLabel() {
    service.getUserInfo { (userInfo) in
      self.descriptionLabel.text = userInfo.battleShip
    }
    descriptionLabel.configurePlainLabel()
  }
  
  private func configureImageView() {
    if InternetReachability.isConnectedToNetwork() == true {
      HUD.show(.progress)
    }
    service.getImages { (images) in
      self.battleShipImgURL = images.battleShipImageURL
      if let imageURL: URL = URL(string: self.battleShipImgURL!) {
        HUD.flash(.success, delay: 0.75)
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.logoImageView.image = UIImage(data: imageData as Data)
      }
    }
    logoImageView.roundedCornerImage()
  }
  
  // MARK: - PageScrollingDelegate
  override func onScrollWithPageRight(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(rotationAngle: -offset * CGFloat.pi)
  }
  
  override func onScrollWithPageLeft(_ offset: CGFloat) {
    self.logoImageView.transform = CGAffineTransform(rotationAngle:
      offset * CGFloat.pi).translatedBy(x: offset * self.view.width,
                                        y: -offset * self.view.height)
    self.descriptionLabel.transform = CGAffineTransform(translationX: self.view.width * offset,
                                                        y: self.view.height * -offset)
  }
}
