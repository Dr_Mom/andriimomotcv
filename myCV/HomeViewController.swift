//
//  HomeViewController.swift
//  myCV
//
//  Created by Andrei Momot on 12/8/16.
//  Copyright © 2016 Dr_Mom. All rights reserved.
//

import UIKit
import PKHUD

class HomeViewController: UIViewController {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var CVImageView: UIImageView!
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var devLabel: UILabel!
  @IBOutlet weak var cellView: UITableView!
  @IBOutlet weak var headerConstraintHeight: NSLayoutConstraint!

  var userInfo: UserInfo?
  var images: UserImages?
  var service = Service()
  var profileImgURL: String?
  var defaultHeaderHeight: CGFloat { return self.view.frame.size.height * 0.4 }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
    configureImageView()
    configureTableView()
    self.headerConstraintHeight.constant = defaultHeaderHeight
  }
  override func viewDidAppear(_ animated: Bool) {
    checkForInternetAvailibility()
  }

  private func configureLabels() {
    service.getUserInfo { (userInfo) in
      self.titleLabel.text = userInfo.name
      self.devLabel.text = userInfo.dev
    }
    titleLabel.configurePlainLabel()
    titleLabel.textColor = UIColor.black
    devLabel.configureSmallLabel()
  }

  private func configureImageView() {
    service.getImages { (images) in
      self.profileImgURL = images.profileImageURL
      if let imageURL: URL = URL(string: self.profileImgURL!) {
        HUD.flash(.success, delay: 0.75)
        guard let imageData: NSData = NSData(contentsOf: imageURL) else {
          print("Image URL is missing")
          return
        }
        self.CVImageView.image = UIImage(data: imageData as Data)
        self.backgroundImageView.image = UIImage(data: imageData as Data)
      }
    }
    CVImageView.layer.borderWidth = 1.0
    CVImageView.roundedImage()
  }

  private func configureTableView() {
    self.cellView.contentInset = UIEdgeInsets(top: defaultHeaderHeight, left: 0, bottom: 0, right: 0)
    self.cellView.tableFooterView = UIView()
    self.cellView.register(PortfolioTableViewCell.self, forCellReuseIdentifier: PortfolioTableViewCell.cellIdentifer)
    self.cellView.separatorColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
    self.cellView.separatorInset = UIEdgeInsets.zero
    self.cellView.layoutMargins = UIEdgeInsets.zero
  }

  private func checkForInternetAvailibility() {
    if InternetReachability.isConnectedToNetwork() != true {
      noInternet()
    }
  }

  private func noInternet() {
    let noInternetVC = UIAlertController(title: "No Internet Connection",
                                         message: "Please, turn on the internet and relaunch the application!",
                                         preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style:.default, handler: nil)
    noInternetVC.addAction(okAction)
    present(noInternetVC, animated: true, completion: nil)
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let offsetY = scrollView.contentOffset.y

    self.headerConstraintHeight.constant = -offsetY

    if -offsetY < self.defaultHeaderHeight {

    let invisiblePoint = self.defaultHeaderHeight - 150

    let newAlpha = (-offsetY - invisiblePoint) / 100
    let labelAlphaOffset: CGFloat = 0.5

    self.titleLabel.alpha = newAlpha - labelAlphaOffset
    self.CVImageView.alpha = newAlpha
    self.devLabel.alpha = newAlpha - labelAlphaOffset
    }
  }
}
