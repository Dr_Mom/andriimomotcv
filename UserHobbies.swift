//
//  Hobbies.swift
//  myCV
//
//  Created by Andrei Momot on 2/22/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

struct UserHobbies {
  var userHobbie: String?

  init(dictionary: [String: Any]) {
    self.userHobbie = dictionary["item"] as? String ?? ""
  }
}
