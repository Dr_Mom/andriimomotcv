//
//  Data.swift
//  myCV
//
//  Created by Andrei Momot on 2/22/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

struct UserInfo {
  var name: String?
  var dev: String?
  var aboutMe: String?
  var aboutMeHeader: String?
  var aboutMeTitle: String?
  var battleShip: String?
  var recipe: String?
  var calculator: String?
  var flappyBird: String?
  var poker: String?
  var skillTitle: String?
  var aboutApp: String?
  var aboutAppHeader: String?
  var univercity: String?
  var speciality: String?
  var graduationDate: String?
  var course1: String?
  var course2: String?
  var course3: String?
  var english1: String?
  var english2: String?
  var english3: String?

  init(dictionary: [String: Any]) {
    self.name = dictionary["name"] as? String ?? ""
    self.dev  = dictionary["dev"] as? String ?? ""
    self.aboutMe = dictionary["aboutMe"] as? String ?? ""
    self.aboutMeHeader = dictionary["aboutMeHeader"] as? String ?? ""
    self.aboutMeTitle = dictionary["aboutMeTitle"] as? String ?? ""
    self.aboutApp = dictionary["aboutApp"] as? String ?? ""
    self.aboutAppHeader = dictionary["aboutAppHeader"] as? String ?? ""
    self.battleShip = dictionary["battleShip"] as? String ?? ""
    self.recipe = dictionary["recipe"] as? String ?? ""
    self.calculator = dictionary["calculator"] as? String ?? ""
    self.flappyBird = dictionary["flappyBird"] as? String ?? ""
    self.poker = dictionary["poker"] as? String ?? ""
    self.skillTitle = dictionary["skillTile"] as? String ?? ""
    self.univercity = dictionary["university"] as? String ?? ""
    self.speciality = dictionary["speciality"] as? String ?? ""
    self.graduationDate = dictionary["graduationDate"] as? String ?? ""
    self.course1 = dictionary["course1"] as? String ?? ""
    self.course2 = dictionary["course2"] as? String ?? ""
    self.course3 = dictionary["course3"] as? String ?? ""
    self.english1 = dictionary["english1"] as? String ?? ""
    self.english2 = dictionary["english2"] as? String ?? ""
    self.english3 = dictionary["english3"] as? String ?? ""
  }
}
