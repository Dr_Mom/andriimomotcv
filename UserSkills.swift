//
//  Skills.swift
//  myCV
//
//  Created by Andrei Momot on 2/22/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

struct UserSkills {
  let userSkill: String

  init(userSkill: String) {
    self.userSkill = userSkill
  }

  init(dictionary: [String: Any]) {
    self.userSkill = dictionary["item"] as? String ?? ""
  }

  func toAnyObject() -> Any {
    return [
      "item": userSkill
    ]
  }
}
