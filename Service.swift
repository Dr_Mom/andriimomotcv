//
//  Service.swift
//  myCV
//
//  Created by Andrei Momot on 2/17/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Service {
  func getUserInfo(completion: @escaping (UserInfo) -> Void) {
    let databaseRef = FIRDatabase.database().reference()
    databaseRef.child("Data").observeSingleEvent(of: .value, with: { (snapshot) in
      if let value = snapshot.value as? [String : AnyObject] {
      let userInfo = UserInfo(dictionary: value)
      completion(userInfo)
      }
    }) { (error) in
    print(error.localizedDescription)
    }
  }

  func getHobbies(completion: @escaping (UserHobbies) -> Void) {
    let databaseRef = FIRDatabase.database().reference()
    databaseRef.child("Hobbies").observe(.childAdded, with: { snapshot in
      if let value = snapshot.value as? [String : AnyObject] {
        let item = UserHobbies(dictionary: value)
        completion(item)
      }
    }) { (error) in
      print(error.localizedDescription)
    }
  }

  func getSkills(completion: @escaping (UserSkills) -> Void) {
    let databaseRef = FIRDatabase.database().reference()
    databaseRef.child("Skills").observe(.childAdded, with: { snapshot in
      if let value = snapshot.value as? [String : AnyObject] {
        let item = UserSkills(dictionary: value)
      completion(item)
      }
    }) { (error) in
      print(error.localizedDescription)
    }
  }

  func postSkill(text: String) {
    let databaseRef = FIRDatabase.database().reference(withPath: "Skills")
    let skillItem = UserSkills(userSkill: text)
    let skillItemRef = databaseRef.child(text)
    skillItemRef.setValue(skillItem.toAnyObject())
  }

  func removeSkill(text: String) {
    let databaseRef = FIRDatabase.database().reference(withPath: "Skills")
    databaseRef.child(text).removeValue()
  }

  func getImages(completion: @escaping (UserImages) -> Void) {
    let databaseRef = FIRDatabase.database().reference()
    databaseRef.child("Images").observeSingleEvent(of: .value, with: { (snapshot) in
      if let value = snapshot.value as? [String : AnyObject] {
        let item = UserImages(dictionary: value)
        completion(item)
      }
    }) { (error) in
      print(error.localizedDescription)
    }
  }
}
